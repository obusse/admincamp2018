var express = require('express');
var app = express();

app.get('/', (request, response) => {
    response.send('Hello World!');
});

app.listen(3000, () => {
    console.log('server is listening on 3000');
});