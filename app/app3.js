var express = require('express');
var app = express();

app.get('/', function(request, response) {
    response.send('Hello World!');
});

app.get('/foo', function(request, response) {
    response.send('foo');
});

app.listen(3000, function() {
    console.log('server is listening on 3000');
});