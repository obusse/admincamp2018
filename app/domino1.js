var shell = require('shelljs');
var express = require('express');
var app = express();

app.get('/', (request, response) => {
    response.send('Available commands: /start /stop');
});

app.get('/start', (request, response) => {
    shell.cd('/local/notesdata');
    shell.exec('/opt/ibm/domino/bin/server');
    shell.echo('Server started');
    response.send('Done.');
});

app.get('/stop', (request, response) => {
    shell.cd('/local/notesdata');
    shell.exec('/opt/ibm/domino/bin/server -q');
    shell.echo('Server stopped');
    response.send('Done.');
});

app.listen(3000, () => {
    console.log('server is listening on 3000');
});