var express = require('express');
var http = require('http');
var app = express();

function createOutput(response) {
    var options = {
        host: 'www.notesx-win.net',
        port: 80,
        path: '/demoapps/addressbook.nsf/api/data/collections/unid/994A57B4048799AEC1258076002AAC26'
    };

    http.get(options, (res) => {
        const { statusCode } = res;
        // this is the same as const statusCode = res.statusCode;

        const contentType = res.headers['content-type'];

        let error;
        if (statusCode !== 200) {
            error = new Error('Request Failed.\n' +
                `Status Code: ${statusCode}`);
        } else if (!/^application\/json/.test(contentType)) {
            error = new Error('Invalid content-type.\n' +
                `Expected application/json but received ${contentType}`);
            response.send(`Expected application/json but received ${contentType}`);
        }
        if (error) {
            console.error(error.message);
            // consume response data to free up memory
            res.resume();
            return;
        }

        res.setEncoding('utf8');
        let rawData = '';
        res.on('data', (chunk) => { rawData += chunk; });
        res.on('end', () => {
            try {
                let output = "<html><body>";
                const parsedData = JSON.parse(rawData);
                for (var record in parsedData) {
                    output += '<div>' + parsedData[record].lastname + '</div>';
                }
                output += '</body></html>';
                response.setHeader('content-type', 'text/html');
                response.status(200).send(output);
            } catch (e) {
                console.error(e.message);
            }
        });
    }).on('error', (e) => {
        console.error(`Got error: ${e.message}`);
        response.send(`Got error: ${e.message}`);
    });
}

app.get('/', (request, response) => {
    createOutput(response);
});

app.listen(3000, () => {
    console.log('server is listening on 3000');
});