<!-- $theme: default -->
<!-- $size: A4 -->

# Node.js f&uuml;r Admins

## Hands-On: jetzt wird's spannend!

<div style="position:fixed;top:20px;right:20px"><img src="images/ac.gif"/></div>

<br/>
<br/>
<div style="float:left;margin-right:20px">
<img src="images/obusse2018_1sw.png" style="width:300px"/>
</div>
<div style="display:inline-block;text-align:right;width:60%">
<p>Oliver Busse</p>
<p>
<span style="font-size:0.7em">We4IT GmbH</span>
<br/>
<a href="https://aveedo.com"><img src="images/aveedo-logo.png" style="width:200px"/></a>
</p>
<p>
<span style="font-size:0.7em">
<a href="http://oliverbusse.com">http://oliverbusse.com</a>
</span>
<br/>
<span style="font-size:0.7em">
<a href="https://twitter.com/zeromancer1972">@zeromancer1972</a></span>
<br/>
<img src="images/ibm-champion-rgb-175px.jpg"/>
</p>
</div>

---

<!-- page_number: true -->

# Agenda

- Transportable Umgebung dank Docker
- Admin-Module (npm)
- Admin-Scripte (node)
- Domino pre v10
- Automation mit Node-RED
- Extra: Smart mit Homebridge

---

<!-- page_number: true -->

# Transportable Umgebung dank Docker

- Vorteile
- Herausforderungen
- Dockerfile
- docker-compose

---

<!-- page_number: true -->

# Vorteile

- schlanke Runtime gegenüber einer Vollinstallation des OS
- skalierbar
- einfaches Deployment
	- lokal vorbereiten, remote einrichten

---

<!-- page_number: true -->

# Herausforderungen

- gewisse Lernkurve des Container-Prinzips
- neue Konfigurationselemente (Dockerfile, yml Files)
- Netzwerkzugriff (exposing)

---

<!-- page_number: true -->

# Dockerfile

- Textdatei im proprietären Format, ähnlich Batchfile
- enthält Angaben zu:
	- Basis OS
	- Workdirs
	- Network
	- Commands
	- Comments
- wird von der docker Binary ausgeführt

---

<!-- page_number: true -->

# Dockerfile - Beispiel

```plaintext
FROM node

RUN mkdir /src

RUN npm install nodemon -g

WORKDIR /src
ADD app/package.json /src/package.json
RUN npm install

ADD app/nodemon.json /src/nodemon.json

EXPOSE 3000

CMD npm start
```

---

<!-- page_number: true -->

# Dockerfile - Ausf&uuml;hrung

```console
$ docker build .
```

Es gibt aber fast unendlich viele Parameter... Vertippen ist vorprogrammiert ;-)

[Docker File Reference](https://docs.docker.com/engine/reference/builder/#usage)

---

<!-- page_number: true -->

# docker-compose

- eigenes Modul in Docker
- nutzt docker-compose.yml als Konfiguration
- Textdatei im YAML Format
- besser lesbar
- schwieriger zu erstellen (Syntax, Linebreaks, Indents)
	- alles hat eine spezielle Aufgabe
- einfachere Bedienung als docker CLI
- nutzt allerdings auch das Dockerfile

---

<!-- page_number: true -->

# docker-compose - Beispiel

```yml
web:
  build: .
  volumes:
    - "./app:/src/app"
  ports:
    - "3030:3000"
  net: "bridge"
```

Der Pfad . bezieht sich auf das aktuelle Verzeichnis, in dem dann das Dockerfile erwartet wird. Entscheidend ist hier die einfache Netzwerk-Konfiguration und das Port-Mapping für die Node.js App.

---

<!-- page_number: true -->

# Admin-Module

- child_process / sys
- shelljs
- commander

---

<!-- page_number: true -->

# child_process / sys

- erlaubt das Ausf&uuml;hren von Consolen-Befehlen (Linux, Windows) in einer Node.js app
- sys vereinfacht Ausgaben unter Linux
- VORSICHT!

---

<!-- page_number: true -->

# child_process / sys - Beispiel

```javascript
// http://nodejs.org/api.html#_child_processes
var sys = require('sys')
var exec = require('child_process').exec;
var child;
// executes `pwd`
child = exec("pwd", function (error, stdout, stderr) {
  sys.print('stdout: ' + stdout);
  sys.print('stderr: ' + stderr);
  if (error !== null) {
    console.log('exec error: ' + error);
  }
});
// or more concisely
var sys = require('sys')
var exec = require('child_process').exec;
function puts(error, stdout, stderr) { sys.puts(stdout) }
exec("ls -la", puts);
```

Quelle: [https://dzone.com/articles/execute-unix-command-nodejs](https://dzone.com/articles/execute-unix-command-nodejs)

---

<!-- page_number: true -->

# shelljs

- Suchen und Ersetzen in Dateien
- Zugriff aufs Filesystem
- Extrahieren von Passagen aus Textdateien
- Bringt viele bekannte Unix-Funktionen mit
	- which
	- sed
	- grep
	- cp
	- mv
	- ...

---

<!-- page_number: true -->

# shelljs - Beispiel (Seite 1)

```javascript
var shell = require('shelljs');

if (!shell.which('git')) {
  shell.echo('Sorry, this script requires git');
  shell.exit(1);
}

// Copy files to release dir
shell.rm('-rf', 'out/Release');
shell.cp('-R', 'stuff/', 'out/Release');
```

... to be continued

---

<!-- page_number: true -->

# shelljs - Beispiel (Seite 2)

```javascript
// Replace macros in each .js file
shell.cd('lib');
shell.ls('*.js').forEach(function (file) {
  shell.sed('-i', 'BUILD_VERSION', 'v0.1.2', file);
  shell.sed('-i', /^.*REMOVE_THIS_LINE.*$/, '', file);
  shell.sed('-i', /.*REPLACE_LINE_WITH_MACRO.*\n/, shell.cat('macro.js'), file);
});
shell.cd('..');

// Run external tool synchronously
if (shell.exec('git commit -am "Auto-commit"').code !== 0) {
  shell.echo('Error: Git commit failed');
  shell.exit(1);
}
```

Quelle: [https://github.com/shelljs/shelljs](https://github.com/shelljs/shelljs)

---

<!-- page_number: true -->

# commander

- Erlaubt die Übergabe von CLI Parameter an Node.js Scripte
- inkl. Hilfe

---

<!-- page_number: true -->

# commander - Beispiel

```javascript
// pizza script
var program = require('commander');
 
program
  .version('0.1.0')
  .option('-p, --peppers', 'Add peppers')
  .option('-P, --pineapple', 'Add pineapple')
  .option('-b, --bbq-sauce', 'Add bbq sauce')
  .option('-c, --cheese [type]', 
  	'Add the specified type of cheese [marble]', 'marble')
  .parse(process.argv);
 
console.log('you ordered a pizza with:');
if (program.peppers) console.log('  - peppers');
if (program.pineapple) console.log('  - pineapple');
if (program.bbqSauce) console.log('  - bbq');
console.log('  - %s cheese', program.cheese);
```

Quelle: [https://www.npmjs.com/package/commander](https://www.npmjs.com/package/commander)

---

<!-- page_number: true -->

# commander - Ausführung

Angenommen, das Node.js Script hei&szlig;t ```pizza.js```

```console
node pizza --cheese Gouda
```

---

<!-- page_number: true -->

# Admin-Scripte

- Domino starten und stoppen
- Consolenbefehle ausf&uuml;hren

---

<!-- page_number: true -->

# Domino starten und stoppen

```plaintext
// start
<dominoProgram>/server

// stop
<dominoProgram>/server -q
```

---

<!-- page_number: true -->

# Domino starten und stoppen mit Node.js (1)

```javascript
var shell = require('shelljs');
var express = require('express');
var app = express();

app.get('/', (request, response) => {
    response.send('Available commands: /start /stop');
});

app.get('/start', (request, response) => {
    shell.cd('/local/notesdata');
    shell.exec('/opt/ibm/domino/bin/server');
    shell.echo('Server started');
    response.send('Done.');
});
// continued on next page
```

---

<!-- page_number: true -->

# Domino starten und stoppen mit Node.js (2)

```javascript
// here we go
app.get('/stop', (request, response) => {
    shell.cd('/local/notesdata');
    shell.exec('/opt/ibm/domino/bin/server -q');
    shell.echo('Server stopped');
    response.send('Done.');
});

app.listen(3000, () => {
    console.log('server is listening on 3000');
});
```

---

<!-- page_number: true -->

# Consolenbefehle ausf&uuml;hren

```javascript
var shell = require('shelljs');

shell.cd('/local/notesdata');
shell.exec('/opt/ibm/domino/bin/server -c \"load compact -B\"');
```

---

<!-- page_number: true -->

# Domino pre V10?

- Domino Apps mit Node.js nutzen
	- Windows only
	- experimental
	- https://github.com/nthjelme/nodejs-domino
- Domino Access Services (DAS)

---

<!-- page_number: true -->

# Domino V10

- die Beta enthält noch kein Node.js :-(
- GA soll Node Stack enthalten
	- Domino als Node server
	- natives Node.js Modul zum Zugriff auf Domino API
- Domino Query Language (DQF) wird direkt unterstützt

---

<!-- page_number: true -->

<img src="images/node-red-icon-2.png" style="width:200px;float:right"/>

# Automation mit Node-RED

- Scripte zeitgesteuert ausführen
- IoT Services nutzen

---

<!-- page_number: true -->

# Was ist Node-RED?

- visueller, Low-Code Editor und Runtime
- Webserver, Gateway
- Programme sind Flows, keine Scripte
- unzählige Plugins für IoT, HTTP, MQTT, Smarthome, Alexa, Watson, ...
- eine Node.js Anwendung :-)

---

<!-- page_number: true -->

# Was ist Node-RED?

<img src="images/node-red.png" style="width:100%"/>

```console
npm install node-red -g --unsafe-perm
```

Empfehlung: mittels pm2 beim Booten starten

---

<!-- page_number: true -->

<img src="images/home.jpg" style="width:200px;float:right"/>

# Extra: Smart mit Homebridge

Oder: wie starte ich den Domino Server mittels iOS neu?

- iOS ist im Business das beliebtere Mobile OS (im Gegensatz zu Android)
- Homekit ist Apples Smart Home Zentrale
- Homebridge ist die Schnittstelle zu Homekit
- Basiert auf Node.js
- Rezepte & Zutaten

---

<!-- page_number: true -->

# Domino Maintenance - aber smart!

- Raspberry Pi mit
	- Node.js
	- Homebridge
	- Node-RED
	- Plugins
- Apple TV oder iPad als Schaltzentrale
- iOS Device (iPhone, iPad)

---

<!-- page_number: true -->

[Dockerizing a Node.js web app](https://nodejs.org/en/docs/guides/nodejs-docker-webapp/)
[Writing Command Line Apps in Node.js](https://medium.freecodecamp.org/writing-command-line-applications-in-nodejs-2cf8327eee2)
[Sources](https://gitlab.com/obusse/admincamp2018)