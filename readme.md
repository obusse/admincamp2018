# AdminCamp 2018 Handouts

This repo contains all materials for the hands-on sessions at AdminCamp 2018.

For the prezis to work you need [Marp](https://yhatt.github.io/marp/), the markdown presentation writer.