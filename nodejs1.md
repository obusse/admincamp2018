<!-- $theme: default -->
<!-- $size: A4 -->

# Node.js f&uuml;r Admins

## Hands-On: Erste Schritte

<div style="position:fixed;top:20px;right:20px"><img src="images/ac.gif"/></div>

<br/>
<br/>
<div style="float:left;margin-right:20px">
<img src="images/obusse2018_1sw.png" style="width:300px"/>
</div>
<div style="display:inline-block;text-align:right;width:60%">
<p>Oliver Busse</p>
<p>
<span style="font-size:0.7em">We4IT GmbH</span>
<br/>
<a href="https://aveedo.com"><img src="images/aveedo-logo.png" style="width:200px"/></a>
</p>
<p>
<span style="font-size:0.7em">
<a href="http://oliverbusse.com">http://oliverbusse.com</a>
</span>
<br/>
<span style="font-size:0.7em">
<a href="https://twitter.com/zeromancer1972">@zeromancer1972</a></span>
<br/>
<img src="images/ibm-champion-rgb-175px.jpg"/>
</p>
</div>

---

<!-- page_number: true -->

# Agenda

- Was ist Node.js?
- Warum Node.js?
- Must-have Tools & Packages
- Einrichten des Systems
- Hello, world (non-persistent)
- Etwas mehr (persistent)
- Module nutzen
- Konfiguration der App (package.json)
- Dependencies
- Express
- Arrow Functions
- App as a Service

---

<!-- page_number: true -->

# Was ist Node.js?

- Serverseitige Javascript Plattform (V8)
	- ES6 Javascript Syntax
	- jeder Web-Developer kann Code lesen und verstehen
- Non-blocking I/O
	- optimiertes Thread-Handling
	- asynchron (sic!)
- High performance framework
- Plattform-unabh&auml;ngig
- **nicht** nur ein Webserver
- kurze Update-Zyklen
- unz&auml;hlige Extensions (Module) verf&uuml;gbar f&uuml;r fast alle Aufgaben

---

<!-- page_number: true -->

# Warum Node.js?

- Node.js ist ist Teil der IBM **NERD** Architectur
	- **N**ode, **E**xpress, **R**eact, **D**omino 10
- Node.js wird damit Teil der "Low-Code" Strategie von Domino 10
- Domino 10 enth&auml;lt ein Node.js Subsystem
- IBM liefert native Node.js Module f&uuml;r die Anbindung von NSF
- Node.js ist schlank, schnell und unendlich flexibel einsetzbar
	- Mehr dazu im 2. Hands-On :-)

---

<!-- page_number: true -->

# Must-have Tools & Packages

- **VS Code** (*der* Editor)
	- sinnvolle Plugins (Linter, Beautifier)
- Commandline-Ersatz (CMDER, iTerm2, ZSH)
- **npm** (Node.js Package Manager)
- **nodemon** (Hotdeploy for Node.js)
- **pm2** (Node.js Process Manager)

---

<!-- page_number: true -->

# Einrichten des Systems (1)

- Node.js herunterladen: [https://nodejs.org/en/download/](https://nodejs.org/en/download/) und installieren
- Commandline/Shell &ouml;ffnen
- Testen:
```plaintext
node -v
```

---

# Einrichten des Systems (2)

- npm installieren bzw. pr&uuml;fen, es ist normalerwiese bei Node.js mit dabei

```plaintext
npm -v
```

- npm = **N**ode **P**ackage **M**anager
- npm verwaltet globale und Projekt-bezogene Module sowie alle Abh&auml;ngigkeiten
- vergleichbar mit apt oder yum in Linux

---

# Einrichten des Systems (3)

- globale Module installieren

```plaintext
npm install -g nodemon pm2
```

- der Schalter -g steht f&uuml;r "global", d.h. systemweit

---

<!-- page_number: true -->

# Hello, world - erste App (non-persistent)

- via Node.js Commandline

```plaintext
node
```

```plaintext
> console.log('Hello, world!')
```

->

```plaintext
Hello, world!
undefined
```

---

# Etwas mehr (persistent)

- Erstelle eine Datei **app.js**
- F&uuml;ge folgende Zeilen ein:

```javascript
console.log('Hello App!');
````

- Starte das Script mit

```plaintext
node app.js
```

- Ausgabe

```plaintext
Hello App!
```

---

# Module nutzen (1)

- &ouml;ffne **app.js**
- Ersetze die bestehende Zeile durch folgenden Code:

```javascript
/* mit require werden Module eingebunden */
var http = require('http');

http.createServer(function(req, res) {
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.end('Hello World!');
}).listen(3000, function() {
    console.log('server is listening on 3000');
});
```

---

# Module nutzen (2)

- Starte die App mit

```plaintext
nodemon app.js
```

- Ausgabe:

```plaintext
server is listening on 3000
```

- &ouml;ffne folgende URL im Browser

```plaintext
http://localhost:3000/
```

- Ausgabe im Browser:

```plaintext
Hello World!
```

- &auml;ndere den Ausgabetext, speichere die &Auml;nderung und lade im Browser die Seite neu, um zu sehen, was ```nodemon``` macht

---

<!-- page_number: true -->

# Konfiguration der App (1)

Node.js Projekte werden i.d.R. mit

```plaintext
npm init
```

eingerichtet.

Damit werden grundlegende Daten der App festgelegt und die Datei

```plaintext
package.json
```

erzeugt.

---

# Konfiguration der App (2)

- package.json

```
{
  "name": "admincamp_demo_app",
  "version": "1.0.0",
  "description": "Dies ist die Demo zum Hands-On vom AdminCamp 2018",
  "main": "app2.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Oliver Busse",
  "license": "ISC"
}
```

- Starte die App mittels ```nodemon``` - es startet die Datei, die in ```main``` definiert ist

---

<!-- page_number: true -->

# Dependencies (1)

- Eine Web-App nutzt i.d.R. das Modul "express"
- F&uuml;hre folgenden Befehl im Verzeichnis der App aus:

```plaintext
npm install express --save
```

- Dies f&uuml;gt eine entsprechenden Eintrag mit allen Dependencies zur ```package.json```hinzu

```json
{
  "name": "admincamp_demo_app",
  ...
  "dependencies": {
    "express": "^4.16.3"
  }
}
```

---

# Dependencies (2)

- Der Schalter ```--save``` f&uuml;gt das Modul der ```package.json``` hinzu, damit geh&ouml;rt es fest zum Projekt
- Das ```^``` Zeichen bedeutet "diese Version oder neuer". F&uuml;r eine statische Version kann das Symbol entfernt werden
- Mittels ```npm install``` kann man alle abh&auml;ngigen Module laden, so dass die App ausgef&uuml;hrt werden kann
- Dies erzeugt einen Ordner ```node_modules``` innerhalb des Projekts
- Dieser sollte *nicht* zu einem GIT Repository hinzugef&uuml;gt werden
- Dazu muss das ```.gitignore``` File folgenden Eintrag aufweisen:

```plaintext
*/node_modules
```

---

<!-- page_number: true -->

# Express (1)

- Das Modul ```express``` bietet noch mehr M&ouml;glichkeiten f&uuml;r den Webserver
- Es kann unterschiedliche Request-Typen unterscheiden (GET, POST, UPDATE, PUT, DELETE...)
- Es unterst&uuml;tzt unterschiedliche Routen f&uuml;r verschiedene URLs

---

# Express (2)

```javascript
var express = require('express');
var app = express();

app.get('/', function (request, response) {
  response.send('Hello World!');
});

app.get('/foo', function (request, response) {
  response.send('Foo bar!');
});

app.listen(3000, function () {
  console.log('server is listening on 3000');
});
```

- Lade ```localhost:3000``` im Browser
- Lade ```localhost:3000/foo``` im Browser

---

# Arrow Functions

- Es kommt vor, dass Scripte mit "Arrow Functions" (=>) geschrieben sind:

```javascript
var express = require('express');
var app = express();

app.get('/', (request, response) => {
    response.send('Hello World!');
});

app.listen(3000, () => {
    console.log('server is listening on 3000');
});
```

- Dies ist eine kurze Schreibweise f&uuml;r ```function()```
- Es gelten etwas andere Regeln bzgl. des Objekt **this** - bitte selbst recherchieren!

---

# App as a Service

- Normalerweise blockiert der Aufruf ```node app.js``` die Comandline
	- f&uuml;r einen Server unbrauchbar, ```pm2```ist Dein Freund!
- ```pm2``` ist ein Process Manager, der eigenst&auml;ndig Prozesse handhabt und bei Bedarf auch neu startet
- ```npm -g install pm2``` installiert den Prozessmanager global
- ```pm2 start app.js``` f&uuml;hrt das Script als Thread aus
- ```pm2 ls``` listet alle aktuellen Prozesse auf
- ```pm2 start <app>``` startet eine App, ```pm2 stop <app>``` stoppt eine App
- ```pm2 save``` speichert den aktuellen Prozesszustand und alle laufenden Prozesse
- ```pm2 startup``` f&uuml;hrt die gepspeicherten Prozesse automatisch beim Booten aus

---

<!-- page_number: true -->

# Ressourcen (1)

[Node.js](https://nodejs.org/)
[NPM](https://www.npmjs.com/)
[VS Code](https://code.visualstudio.com/)
[CMDER](http://cmder.net/)
[iTerm2](https://iterm2.com/)
[ZSH](https://ohmyz.sh/)
[NPM](https://www.npmjs.com/get-npm)
[nodemon](https://www.npmjs.com/package/nodemon)
[pm2](https://www.npmjs.com/package/pm2)
[Tim Davis' intro to Node.js](https://turtleblog.info/2018/06/12/domino-10-vs-nosql/)
[Tim Davis' on variables](https://turtleblog.info/2018/09/03/things-to-know-with-javascript-json-let-const-and-arrows/)
[Sources](https://gitlab.com/obusse/admincamp2018)